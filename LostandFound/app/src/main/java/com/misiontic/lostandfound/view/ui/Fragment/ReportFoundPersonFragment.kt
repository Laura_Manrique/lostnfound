package com.misiontic.lostandfound.view.ui.Fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.example.tiendageli.view.ui.network.FirestoreService
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.firestore.FirebaseFirestore
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.databinding.FragmentReportFoundPersonBinding
import com.misiontic.lostandfound.model.FoundPerson
import java.text.SimpleDateFormat
import java.util.*

class ReportFoundPersonFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private lateinit var binding: FragmentReportFoundPersonBinding
    private val repository = FirestoreService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentReportFoundPersonBinding.inflate(inflater, container, false)

        binding.btnReportFoundPerson.setOnClickListener {
            setFoundPerson()
        }
        return binding.root
    }

    fun setFoundPerson(){
        binding.progressFoundReport.visibility = View.VISIBLE
        //binding.btnReportFoundPerson.isEnabled = false
        var image = ""
        var nombre = ""
        val db = FirebaseFirestore.getInstance()
        db.collection("Desaparecidos")
            .document(binding.etNumDocFoundPersonReport.text.toString())
            .get()
            .addOnSuccessListener { document ->
                if (document.exists()){
                    val c: Calendar = Calendar.getInstance()
                    val sdf = SimpleDateFormat("dd/MM/yyyy")
                    val strDate: String = sdf.format(c.getTime())

                    val img = document.data?.get("userImageUrl").toString()
                    val name = document.data?.get("nombre").toString()

                    repository.postFoundPerson(
                        FoundPerson(
                            binding.etUbiPersonaReporte.text.toString(),
                            name,
                            binding.etEdadFoundPersonReport.text.toString(),
                            strDate,
                            binding.etEstaturaFoundPersonReport.text.toString(),
                            binding.etNumDocFoundPersonReport.text.toString(),
                            img,
                            binding.etColorFoundPersonReport.text.toString(),
                            binding.etSexoFoundPersonReport.text.toString(),
                            binding.etAddressFoundPersonReport.text.toString(),
                            binding.etPhoneFoundPersonReport.text.toString(),
                            binding.etNamePersonReport.text.toString(),
                            binding.etNumDocFoundPersonReport.text.toString(),
                            binding.etTypeSangrePersonReport.text.toString()
                        )
                    )
                    Toast.makeText(activity, "¡La persona se ha reportado exitosamente!", Toast.LENGTH_SHORT).show()
                    findNavController().navigate(R.id.foundPersonsFragment2)

                }
            }


    }
}