package com.misiontic.lostandfound.view.ui.Fragment

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.adapter.AdapterPersons
import com.misiontic.lostandfound.databinding.FragmentDesaparecidosBinding
import com.misiontic.lostandfound.model.PersonaDesaparecida
import com.misiontic.lostandfound.viewmodel.PersonsViewModel

class DesaparecidosFragment : Fragment(),  AdapterPersons.onPersonClickListener, SearchView.OnQueryTextListener{
    private lateinit var binding: FragmentDesaparecidosBinding
    private lateinit var adapter: AdapterPersons
    private val viewModel by lazy{ ViewModelProvider(this).get(PersonsViewModel::class.java)}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDesaparecidosBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = AdapterPersons(this)
        binding.rvDesaparecidos.layoutManager = LinearLayoutManager(activity)
        binding.rvDesaparecidos.adapter = adapter

        binding.floatingReport.setOnClickListener {
            findNavController().navigate(R.id.action_desaparecidosFragment_to_addReportPersonFragment)
        }

        observeData()

        binding.searchDesaparecidos.setOnQueryTextListener(this)

    }

    private fun observeData() {
        viewModel.fetchPersonsData().observe(viewLifecycleOwner, Observer {
            adapter.setListData(it)
            adapter.notifyDataSetChanged()
        })
    }

    override fun onItemClick(item: PersonaDesaparecida) {
        val action = DesaparecidosFragmentDirections.actionDesaparecidosFragmentToDetallePersonaDesaparecidaFragment(item)
        findNavController().navigate(action)
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onQueryTextChange(p0: String?): Boolean {
        adapter.filtro(p0.toString())
        return false

    }


}