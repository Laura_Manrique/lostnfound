package com.misiontic.lostandfound.model

data class User(
    var name :String? = null,
    var email :String? = null,
    var num_doc :String? = null,
    var name_entidad :String? = null,
    var address_entidad :String? = null,
    var city: String? = null,
    var phone :String? = null
)
