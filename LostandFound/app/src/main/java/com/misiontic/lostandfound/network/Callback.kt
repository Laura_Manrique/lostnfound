package com.example.tiendageli.view.ui.network

import java.lang.Exception

//Este es un callback que se usa para los request de firebase
interface Callback<T> {
    fun onSuccess(result: T?)
    fun onFailed(exception: Exception)
}