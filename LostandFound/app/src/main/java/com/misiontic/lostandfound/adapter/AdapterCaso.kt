package com.misiontic.lostandfound.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.adapter.base.BaseViewHolder
import com.misiontic.lostandfound.model.Caso
import com.misiontic.lostandfound.model.FoundPerson
import com.misiontic.lostandfound.model.PersonaDesaparecida
import com.misiontic.lostandfound.view.ui.Fragment.CasoFragment
import java.lang.IllegalArgumentException
import java.util.stream.Collectors

class AdapterCaso(): RecyclerView.Adapter<BaseViewHolder<*>>(){
    private var dataList = mutableListOf<Caso>()
    private var dataListFilter = mutableListOf<Caso>()


    fun setListData(data: MutableList<Caso>){
        dataList = data
        dataListFilter.addAll(dataList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return PersonViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_caso, parent, false))

    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder){
            is PersonViewHolder -> holder.bind(dataList[position], position)
            else -> throw IllegalArgumentException("Error")
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun filtro(texto: String){

        if(texto.length == 0){
            dataList.clear()
            dataList.addAll(dataListFilter)
        }else{
            var collection = dataList.stream().filter {
                it.nombre.uppercase().contains(texto.uppercase())
            }.collect(Collectors.toList())
            dataList.clear()
            dataList.addAll(collection)

        }
        notifyDataSetChanged()

    }

    override fun getItemCount(): Int = dataList.size

    inner class PersonViewHolder(itemView: View):BaseViewHolder<Caso>(itemView){
        override fun bind(item: Caso, position: Int) {
            Glide.with(itemView.context).load(item.userImageUrl).into(itemView.findViewById(R.id.itemImg))

            itemView.findViewById<TextView>(R.id.chipDateAntes).text = item.fechaDesaparicion
            itemView.findViewById<TextView>(R.id.chipDateDespues).text = item.fecha_encontrado
            itemView.findViewById<TextView>(R.id.itemNameCaso).text = item.nombre
            itemView.findViewById<TextView>(R.id.tvNamePersonReport).text = item.name_person_report
        }

    }
}