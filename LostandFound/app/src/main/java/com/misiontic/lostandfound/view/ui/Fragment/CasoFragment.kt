package com.misiontic.lostandfound.view.ui.Fragment

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.misiontic.lostandfound.adapter.AdapterCaso
import com.misiontic.lostandfound.databinding.FragmentCasoBinding
import com.misiontic.lostandfound.model.Caso
import com.misiontic.lostandfound.model.FoundPerson
import com.misiontic.lostandfound.viewmodel.FoundPersonViewModel
import com.misiontic.lostandfound.viewmodel.PersonsViewModel


class CasoFragment : Fragment() , SearchView.OnQueryTextListener{

    private val viewModel by lazy{ ViewModelProvider(this).get(PersonsViewModel::class.java)}
    private val viewModel2 by lazy{ ViewModelProvider(this).get(FoundPersonViewModel::class.java)}
    private lateinit var binding: FragmentCasoBinding
    private lateinit var adapter: AdapterCaso
    private var listCaso: MutableList<Caso> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCasoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.searchCaso.setOnQueryTextListener(this)


        adapter = AdapterCaso()
        binding.rvCasos.layoutManager = LinearLayoutManager(activity)
        binding.rvCasos.adapter = adapter

        observeData()
    }

    private fun observeData() {

        viewModel.fetchPersonsData().observe(viewLifecycleOwner, Observer { personas1 ->
            viewModel2.fetchFoundPerson().observe(viewLifecycleOwner, Observer { personas2 ->
                personas1.forEach { persona1 ->
                    personas2.forEach { persona2 ->
                        if(persona1.cedula.equals(persona2.num_documento)){


                            listCaso.add(Caso(
                                persona2.name.toString(),
                                persona2.edad.toString(),
                                persona2.num_documento.toString(),
                                persona1.fechaDesaparicion.toString(),
                                persona2.fecha_encontrado,
                                persona2.sexo.toString(),
                                persona2.estatura.toString(),
                                persona2.color_piel.toString(),
                                persona1.ciudadResidencia.toString(),
                                persona2.ubi_encontrado,
                                persona1.rh.toString(),
                                persona1.userImageUrl,
                                persona2.phone_contact.toString(),
                                persona2.name_person_report.toString(),
                                persona2.num_doc_person_report,
                                persona2.tipo_sangre_person_report
                            ))
                        }
                    }

                }
                Log.w("lista Casos", "${listCaso}")
                adapter.setListData(listCaso)
                adapter.notifyDataSetChanged()
            })




        })
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onQueryTextChange(p0: String?): Boolean {
        adapter.filtro(p0.toString())
        return false
    }


}