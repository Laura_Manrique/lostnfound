package com.misiontic.lostandfound.model

data class Contact(
    var nombre: String? = null,
    var telefono: String? = null,
    var email: String? = null,
)
