package com.misiontic.lostandfound.view.ui.Fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.findNavController
import com.example.tiendageli.view.ui.network.FirestoreService
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.databinding.FragmentAddReportPersonBinding
import com.misiontic.lostandfound.model.PersonaDesaparecida
import java.util.*

class AddReportPersonFragment : Fragment() {

    private val GALERY_INTENT = 1
    private lateinit var binding: FragmentAddReportPersonBinding
    private lateinit var imgR: Uri
    private val repo = FirestoreService()

    val storage: StorageReference = FirebaseStorage.getInstance().getReference()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddReportPersonBinding.inflate(inflater, container, false)


        binding.btnSelectDate.setOnClickListener {
            val dialogFecha = DatePickerFragment{year, mes, dia -> mostrarResultado(year, mes, dia) }
            dialogFecha.showNow(parentFragmentManager, "Fecha")
        }

        binding.btnUpImg.setOnClickListener { requestPermission() }

        binding.btnReportPerson.setOnClickListener {
            upPerson()
        }


        return binding.root
    }

    private fun mostrarResultado(year: Int, mes: Int, dia: Int) {
        binding.etFechaPersonaReporte.setText("$dia/$mes/$year")
    }

    private fun requestPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            when{
                ContextCompat.checkSelfPermission(
                    activity!!,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED ->{
                    pickPhothoFromGalery()
                }
                else -> requestPermissionLauncher.launch(android.Manifest.permission.READ_EXTERNAL_STORAGE)
            }
        }else{
            pickPhothoFromGalery()
        }
    }
    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ){
        if(it){
            pickPhothoFromGalery()
        }else{
            Toast.makeText(activity, "Necesita activar permisos", Toast.LENGTH_SHORT).show()
        }
    }

    private val startForActivityGallery = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ){result ->
        if(result.resultCode == Activity.RESULT_OK){
            imgR = result.data?.data!!
            binding.imgReporte.setImageURI(imgR)
        }

    }

    private fun pickPhothoFromGalery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startForActivityGallery.launch(intent)
    }
    private fun upPerson(){
        val filePath = storage.child("fotosDesaparecidos").child(imgR?.lastPathSegment!!)
        binding.progressReport.visibility = View.VISIBLE
        var sexoValue = "F"
        if(binding.radiuM.isChecked){
            sexoValue = "M"
        }

        binding.btnUpImg.isEnabled = false
        binding.btnReportPerson.isEnabled = false
        filePath.putFile(imgR).continueWith {
            if (!it.isSuccessful) {
                it.exception?.let { t ->
                    throw t
                }
            }
            filePath.downloadUrl
        }.addOnCompleteListener {
            if(it.isSuccessful){
                it.result!!.addOnSuccessListener{task ->
                    val myUri = task.toString()
                    Log.d("se subío la foto","valor $myUri")
                    binding.progressReport.visibility = View.GONE
                    if(repo.setPerson(
                            PersonaDesaparecida(
                                binding.etNombrePersonaReporte.text.toString(),
                                binding.etEdadPersonaReporte.text.toString(),
                                binding.etNumCedulaPersonaReporte.text.toString(),
                                binding.etFechaPersonaReporte.text.toString(),
                                sexoValue,
                                binding.etEstaturaPersonaReporte.text.toString(),
                                binding.etColorPielPersonaReporte.text.toString(),
                                binding.etCiudadPersonaReporte.text.toString(),
                                binding.etRHPersonaReporte.text.toString(),
                                myUri
                            )
                        )){
                        Toast.makeText(activity, "¡La persona se ha publicado!", Toast.LENGTH_SHORT).show()
                        findNavController().navigate(R.id.desaparecidosFragment)
                    }

                    binding.btnUpImg.isEnabled = true
                    binding.btnReportPerson.isEnabled = true
                    //ojito con el error aquí de precio
                }
            }
        }
    }

    class DatePickerFragment(val listener: (year: Int, mes :Int, dia: Int) -> Unit): DialogFragment(), DatePickerDialog.OnDateSetListener{

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val mes = c.get(Calendar.MONTH)
            val dia = c.get(Calendar.DAY_OF_MONTH)


            return DatePickerDialog(requireActivity(), this,year,mes, dia)
        }
        override fun onDateSet(p0: DatePicker?, year: Int, mes: Int, dia: Int) {
            listener(year, mes, dia)
        }

    }


}