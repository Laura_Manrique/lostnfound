package com.misiontic.lostandfound

import android.app.Application
import com.google.android.material.color.DynamicColors

class LostAndFound : Application(){
    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)
    }
}