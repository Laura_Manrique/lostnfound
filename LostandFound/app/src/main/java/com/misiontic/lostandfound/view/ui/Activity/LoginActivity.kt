package com.misiontic.lostandfound.view.ui.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.databinding.ActivityLoginBinding
import com.misiontic.lostandfound.view.ui.MainActivity

class LoginActivity : AppCompatActivity() {
    private val TAG = "LOGIN_TAG"
    private lateinit var  binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val auth = Firebase.auth

        if(auth.currentUser != null){
            goHomeActivity()
        }

        binding.loginButton?.setOnClickListener {
            binding.loginButton?.isEnabled = false
            val email = binding.etEmail?.text.toString()
            val password = binding.etPassword?.text.toString()
            if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "El correo electrónico/contraseña no deben estar vacíos", Toast.LENGTH_SHORT).show()
                    binding.loginButton?.isEnabled = true
                    return@setOnClickListener
                }
            // Firebase authentication check
            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                    binding.loginButton?.isEnabled = true
                    if (task.isSuccessful) {
                            Toast.makeText(this, "Inicio de Sesión Exitoso!", Toast.LENGTH_SHORT).show()
                            if (auth.currentUser != null) {
                                    goHomeActivity()
                                }
                        } else {
                            Log.e(TAG, "Error al intentar ingresar", task.exception)
                            Toast.makeText(this, "Atenticación Fallida, Intentelo de nuevo", Toast.LENGTH_SHORT).show()
                        }
                }
        }

        binding.registerEntityButton?.setOnClickListener {
            goRegisterActivity()
        }
    }

    private fun goHomeActivity() {
        Log.i(TAG, "goHomeActivity")
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun goRegisterActivity() {
        Log.i(TAG, "goRegisterActivity")
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }
}