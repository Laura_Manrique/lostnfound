package com.misiontic.lostandfound.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.adapter.base.BaseViewHolder
import com.misiontic.lostandfound.model.PersonaDesaparecida
import com.misiontic.lostandfound.view.ui.Fragment.DesaparecidosFragment
import java.lang.IllegalArgumentException
import java.util.stream.Collectors

class AdapterPersons(private val itemClickListener: DesaparecidosFragment): RecyclerView.Adapter<BaseViewHolder<*>>(){
    private var dataList = mutableListOf<PersonaDesaparecida>()
    private var dataListFilter = mutableListOf<PersonaDesaparecida>()



    interface onPersonClickListener{
        fun onItemClick(item: PersonaDesaparecida)
    }


    fun setListData(data:MutableList<PersonaDesaparecida>){
        dataList = data
        dataListFilter.addAll(dataList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return PersonViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false))

    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder){
            is PersonViewHolder -> holder.bind(dataList[position], position)
            else -> throw IllegalArgumentException("Error")
        }
    }


    @RequiresApi(Build.VERSION_CODES.N)
    fun filtro(texto: String){

        if(texto.length == 0){
            dataList.clear()
            dataList.addAll(dataListFilter)
        }else{
            var collection = dataList.stream().filter {
                it.nombre.uppercase().contains(texto.uppercase())
            }.collect(Collectors.toList())
            dataList.clear()
            dataList.addAll(collection)

        }
        notifyDataSetChanged()

    }

    override fun getItemCount(): Int = dataList.size

    inner class PersonViewHolder(itemView: View):BaseViewHolder<PersonaDesaparecida>(itemView){
        override fun bind(item: PersonaDesaparecida, position: Int) {
            Glide.with(itemView.context).load(item.userImageUrl).into(itemView.findViewById(R.id.itemImg))
           // Picasso.get().load(item.foto).into(itemView.findViewById(R.id.itemImg))


            //cuando selecciona cada card de persona desaparecida
            itemView.setOnClickListener{
                itemClickListener.onItemClick(item)
            }
            itemView.findViewById<Chip>(R.id.chipCiudad).text = item.ciudadResidencia
            itemView.findViewById<TextView>(R.id.itemName).text = item.nombre
            itemView.findViewById<TextView>(R.id.itemAge).text = "Edad: ${item.edad}"
            itemView.findViewById<TextView>(R.id.itemGender).text = "Sexo: ${item.sexo}"
            itemView.findViewById<TextView>(R.id.itemRh).text = "RH: ${item.rh}"
        }

    }
}