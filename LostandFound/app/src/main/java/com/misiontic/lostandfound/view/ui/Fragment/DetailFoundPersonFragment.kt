package com.misiontic.lostandfound.view.ui.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.misiontic.lostandfound.databinding.FragmentDetailFoundPersonBinding
import com.misiontic.lostandfound.model.FoundPerson
import com.misiontic.lostandfound.view.ui.Fragment.DetailFoundPersonFragmentArgs.*

class DetailFoundPersonFragment : BottomSheetDialogFragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentDetailFoundPersonBinding
    private lateinit var foundPeople: FoundPerson

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        foundPeople = fromBundle(requireArguments()).foundperson
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDetailFoundPersonBinding.inflate(inflater, container, false)
        setData()
        return binding.root
    }

    private fun setData() {
        binding.tvUbicacionDetail.text = foundPeople.ubi_encontrado
        binding.tvNombreDetail.text = foundPeople.name
        binding.tvEdadDetail.text = foundPeople.edad
        binding.tvEstaturaDetail.text = foundPeople.estatura
        binding.tvNumDocumentoDetail.text = foundPeople.num_documento
        Glide.with(binding.root.context).load(foundPeople.picture).into(binding.imgFoundPerson)
        binding.tvColorPielDetail.text = foundPeople.color_piel
        binding.tvSexoDetail.text = foundPeople.sexo
        binding.tvAddressActualDetail.text = foundPeople.address_actual
        binding.tvPhoneContactDetail.text = "+57 ${foundPeople.phone_contact}"
        binding.tvNamePersonReportDetail.text = foundPeople.name_person_report
        binding.tvDocPersonReportDetail.text = foundPeople.num_doc_person_report
        binding.tvRhPersonReportDetail.text = foundPeople.tipo_sangre_person_report


    }

}