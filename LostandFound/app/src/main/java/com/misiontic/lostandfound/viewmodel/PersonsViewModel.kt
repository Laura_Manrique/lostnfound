package com.misiontic.lostandfound.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.example.tiendageli.view.ui.network.FirestoreService
import com.misiontic.lostandfound.model.PersonaDesaparecida

class PersonsViewModel : ViewModel()  {
    private val repo = FirestoreService()
    private val viewModelFound = FoundPersonViewModel()

    fun fetchPersonsData(): LiveData<MutableList<PersonaDesaparecida>> {
        val mutableData = MutableLiveData<MutableList<PersonaDesaparecida>>()
        repo.getPersonsData().observeForever{
            mutableData.value = it
        }
        return mutableData
    }
}