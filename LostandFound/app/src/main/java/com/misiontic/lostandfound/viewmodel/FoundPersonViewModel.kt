package com.misiontic.lostandfound.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tiendageli.view.ui.network.FirestoreService
import com.misiontic.lostandfound.model.FoundPerson

class FoundPersonViewModel :ViewModel(){
    private val repository = FirestoreService()
    fun fetchFoundPerson(): LiveData<MutableList<FoundPerson>> {
        val mutableData = MutableLiveData<MutableList<FoundPerson>>()
        repository.getFoundPersonsData().observeForever { mutableData.value = it }
        return mutableData
    }
}