package com.misiontic.lostandfound.view.ui.Fragment

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.adapter.AdapterFoundPersons
import com.misiontic.lostandfound.databinding.FragmentFoundPersonsBinding
import com.misiontic.lostandfound.model.FoundPerson
import com.misiontic.lostandfound.viewmodel.FoundPersonViewModel

class FoundPersonsFragment : Fragment(), AdapterFoundPersons.onFoundPersonClickListener, SearchView.OnQueryTextListener {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentFoundPersonsBinding
    private lateinit var adapterFound : AdapterFoundPersons
    private val viewModelFoundPersons by lazy {ViewModelProvider(this).get(FoundPersonViewModel::class.java)}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFoundPersonsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapterFound = AdapterFoundPersons(this)
        binding.rvFoundPersons.layoutManager = LinearLayoutManager(context)
        binding.rvFoundPersons.adapter = adapterFound

        binding.floatingReportFoundPerson.setOnClickListener {
            findNavController().navigate(R.id.action_foundPersonsFragment2_to_reportFoundPersonFragment)
        }

        observeData()

        binding.searchFoundPersons.setOnQueryTextListener(this)
    }

    private fun observeData(){
        viewModelFoundPersons.fetchFoundPerson().observe(viewLifecycleOwner, Observer{
            adapterFound.setListData(it)
            adapterFound.notifyDataSetChanged()
        })
    }

    override fun onItemClick(item: FoundPerson) {
        val action = FoundPersonsFragmentDirections.actionFoundPersonsFragment2ToDetailFoundPersonFragment(item)
        findNavController().navigate(action)
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onQueryTextChange(p0: String?): Boolean {
        adapterFound.filtro(p0.toString())
        return false
    }


}