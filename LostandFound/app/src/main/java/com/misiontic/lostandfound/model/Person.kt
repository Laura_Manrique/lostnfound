package com.misiontic.lostandfound.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PersonaDesaparecida(

    val nombre:String = "",
    val edad:String = "",
    val cedula:String = "",
    val fechaDesaparicion: String = "",
    val sexo: String ="",
    val estatura: String = "",
    val colorPiel: String = "",
    val ciudadResidencia: String = "",
    val rh: String = "",
    val userImageUrl: String = "",
//    val contacto: Contacto?

): Parcelable
