package com.misiontic.lostandfound.view.ui.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.tiendageli.view.ui.network.CONTACT_LOST_PERSONS_COLLECTION_NAME
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.firestore.FirebaseFirestore
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.databinding.FragmentDetallePersonaDesaparecidaBinding
import com.misiontic.lostandfound.model.Contact
import com.misiontic.lostandfound.model.PersonaDesaparecida
import com.misiontic.lostandfound.view.ui.Fragment.DetallePersonaDesaparecidaFragmentArgs.*

class DetallePersonaDesaparecidaFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentDetallePersonaDesaparecidaBinding
    //personaDesaparecida
    private lateinit var personaD: PersonaDesaparecida
    var doc = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        personaD = fromBundle(requireArguments()).person
        Log.d("PersonaDetalle", "Persona ${personaD.nombre}")
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetallePersonaDesaparecidaBinding.inflate(inflater, container, false)
        cargarValores()
        binding.btnContactDetail.setOnClickListener {
            setContactDesaparecido()
        }
        return binding.root
    }

    fun cargarValores(){

        Glide.with(binding.root.context).load(personaD.userImageUrl).into(binding.imgDetalle)
        binding.tvNombreDetail.text = personaD.nombre
        binding.tvEdadDetalle.text = "Edad: ${personaD.edad} años"
        binding.tvCedulaDetalle.text = "CC: ${personaD.cedula}"
        binding.tvColorPielDetalle.text = "Color de piel: ${personaD.colorPiel}"
        binding.tvCiudadDetalle.text = "Ciudad: ${personaD.ciudadResidencia}"
        binding.tvFechaDetalle.text = "Fecha de desaparición: ${personaD.fechaDesaparicion}"
        binding.tvEstaturaDetalle.text = "Estatura: ${personaD.estatura} m"
        binding.tvRHDetalle.text = "RH: ${personaD.rh}"
        binding.tvSexo.text = "Sexo: ${personaD.sexo}"

        doc = personaD.cedula
    }

    fun setContactDesaparecido(){
        val lost_person_contact = Contact()
        lost_person_contact.nombre = binding.etNameContactDetail.text.toString()
        lost_person_contact.telefono = binding.etPhoneContactDetail.text.toString()
        lost_person_contact.email = binding.etEmailContactDetail.text.toString()

        val firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseFirestore.collection("Desaparecidos")
            .document(doc).collection(CONTACT_LOST_PERSONS_COLLECTION_NAME).add(lost_person_contact)
            .addOnSuccessListener {
                Log.d("Contacto", "DocumentSnapshot added with ID: ${it.id}")
                Toast.makeText(context, "Contacto agregado", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.desaparecidosFragment)
            }.addOnFailureListener {
                Log.w("Contacto", "Error adding document", it)
            }


    }



}