package com.misiontic.lostandfound.model

data class Caso(
    val nombre:String = "",
    val edad:String = "",
    val cedula:String = "",
    val fechaDesaparicion: String = "",
    var fecha_encontrado: String? = null,
    val sexo: String ="",
    val estatura: String = "",
    val colorPiel: String = "",
    val ciudadResidencia: String = "",
    var ubi_encontrado: String? = null,
    val rh: String = "",
    val userImageUrl: String = "",
    var phone_contact: String? = null,
    var name_person_report: String? = null,
    var num_doc_person_report: String? = null,
    var tipo_sangre_person_report: String? = null

)
