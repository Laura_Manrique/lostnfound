package com.misiontic.lostandfound.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.adapter.base.BaseViewHolder
import com.misiontic.lostandfound.model.Contact

class AdapterContact():RecyclerView.Adapter<BaseViewHolder<*>>() {
    private var dataList = mutableListOf<Contact>()

    fun setListData(data:MutableList<Contact>){
        dataList = data
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return ContactViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_card_contact,parent,false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder){
            is ContactViewHolder -> holder.bind(dataList[position],position)
            else -> throw IllegalArgumentException("Error")
        }
    }

    override fun getItemCount(): Int = dataList.size

    inner class ContactViewHolder(itemView: View):BaseViewHolder<Contact>(itemView){
        override fun bind(item: Contact, position: Int) {
        }
    }
}