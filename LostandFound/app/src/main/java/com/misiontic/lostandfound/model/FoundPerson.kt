package com.misiontic.lostandfound.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FoundPerson(
    var ubi_encontrado: String? = null,
    var name: String? = null,
    var edad: String? = null,
    var fecha_encontrado: String? = null,
    var estatura: String? = null,
    var num_documento: String? = null,
    var picture: String? = null,
    var color_piel: String? = null,
    var sexo: String? = null,
    var address_actual: String? = null,
    var phone_contact: String? = null,
    var name_person_report: String? = null,
    var num_doc_person_report: String? = null,
    var tipo_sangre_person_report: String? = null
): Parcelable
