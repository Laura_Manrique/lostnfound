package com.misiontic.lostandfound.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.adapter.base.BaseViewHolder
import com.misiontic.lostandfound.model.FoundPerson
import com.misiontic.lostandfound.view.ui.Fragment.FoundPersonsFragment
import java.util.stream.Collectors

class AdapterFoundPersons(private val itemClickListener: FoundPersonsFragment): RecyclerView.Adapter<BaseViewHolder<*>>() {
    private var dataList = mutableListOf<FoundPerson>()
    private var dataListFilter = mutableListOf<FoundPerson>()

    interface onFoundPersonClickListener{
        fun onItemClick(item: FoundPerson)
    }

    fun setListData(data: MutableList<FoundPerson>) {
        dataList = data
        dataListFilter.addAll(dataList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return FoundPersonViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_card_found_person, parent, false))
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun filtro(texto: String){
        if(texto.length == 0){
            dataList.clear()
            dataList.addAll(dataListFilter)
        }else{
            var collection = dataList.stream().filter {
                it.name?.uppercase()!!.contains(texto.uppercase())
            }.collect(Collectors.toList())
            dataList.clear()
            dataList.addAll(collection)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is FoundPersonViewHolder -> holder.bind(dataList[position], position)
            else -> throw IllegalArgumentException("Error")
        }
    }

    override fun getItemCount(): Int = dataList.size

    inner class FoundPersonViewHolder(itemView: View): BaseViewHolder<FoundPerson>(itemView) {
        override fun bind(item: FoundPerson, position: Int) {

            itemView.setOnClickListener{
                itemClickListener.onItemClick(item)
            }
            Glide.with(itemView.context).load(item.picture).into(itemView.findViewById(R.id.itemImgFondPerson))
            itemView.findViewById<android.widget.TextView>(R.id.itemNameFondPerson).text = item.name
            itemView.findViewById<android.widget.TextView>(R.id.itemUbication).text = "Ubicación: ${item.ubi_encontrado}"
            itemView.findViewById<android.widget.TextView>(R.id.itemPhone).text = "N° Celular: +57${item.phone_contact}"
            itemView.findViewById<Chip>(R.id.chipDocFondPerson).text = "id:${item.num_documento}"
        }
    }
}