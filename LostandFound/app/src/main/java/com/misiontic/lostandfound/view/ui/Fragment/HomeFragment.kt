package com.misiontic.lostandfound.view.ui.Fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.tiendageli.view.ui.network.FirestoreService
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.databinding.FragmentDesaparecidosBinding
import com.misiontic.lostandfound.databinding.FragmentHomeBinding
import com.misiontic.lostandfound.model.PersonaDesaparecida
import com.misiontic.lostandfound.view.ui.Activity.LoginActivity
import com.misiontic.lostandfound.viewmodel.PersonsViewModel

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var repo: FirestoreService
    private var listPerson = mutableListOf<PersonaDesaparecida>()
    private val viewModel by lazy { ViewModelProvider(this).get(PersonsViewModel::class.java) }
    private lateinit var barChart: BarChart

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPieChart()
        barChart = view.findViewById(R.id.graph_barchart)
        binding.btnSignOut.setOnClickListener {
            Firebase.auth.signOut()
            val intent = Intent(activity, LoginActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }
        setbarChart()
        //   if (piechart != null) {
        //piechart.getChildVisibleRect(piechart)
        // }
    }

    fun setPieChart() {
        var coutnF = 0
        var coutnM = 0
        var coutnD = 0
        val piechartentry = ArrayList<PieEntry>()
      /*  viewModel.fetchPersonsData().observe(viewLifecycleOwner, Observer {
            it.forEach {
                if (it.sexo.uppercase().contains("F")) {
                    Log.d("F", "cantidad ${coutnF}")
                    coutnF++

                }
            }
        })*/

        // values
        piechartentry.add(PieEntry(7f, "Masculino"))
        piechartentry.add(PieEntry(3f,"Femenino"))
        piechartentry.add(PieEntry(1f,"N/A"))

        //colors
        val colors = ArrayList<Int>()
        colors.add(Color.BLACK)
        colors.add(Color.BLUE)
        colors.add(Color.RED)

        // fill the chart
        val piedataset = PieDataSet(piechartentry, "Género")
        //piedataset.color = resources.getColor(R.color.green)
        piedataset.colors = colors
        piedataset.sliceSpace = 3f

        val data = PieData(piedataset)
        binding.graphPiechart.data = data
        binding.graphPiechart.holeRadius = 5f
        binding.graphPiechart.setBackgroundColor(resources.getColor(R.color.seed))
        binding.graphPiechart.setEntryLabelTextSize(13f)
        binding.graphPiechart.animateY(1000)

    }

    fun setbarChart(){

        // values
        val barchartentry = ArrayList<String>()
        barchartentry.add("Bogotá")
        barchartentry.add("Cartagena")
        barchartentry.add("Tunja")
        barchartentry.add("Medellín")
        barchartentry.add("Cali")


        var valores = ArrayList<BarEntry>()
        valores.add (BarEntry(3f, 0f))
        valores.add(BarEntry(4f, 1f))
        valores.add(BarEntry(1f, 2f))
        valores.add(BarEntry(2f, 3f))
        valores.add(BarEntry(1f, 4f))

        val bardataset = BarDataSet(valores,"Bogotá")
        bardataset.color = resources.getColor(R.color.md_theme_dark_primary)
        val data = BarData(bardataset)
        barChart.data = data
        barChart.animateXY(3000,3000)


    }

}

