package com.example.tiendageli.view.ui.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.google.firebase.ktx.Firebase
import com.misiontic.lostandfound.model.FoundPerson
import com.misiontic.lostandfound.model.PersonaDesaparecida
import java.lang.Exception

const val USERS_COLLECTION_NAME = "users"
const val FOUND_PERSONS_COLLECTION_NAME = "found_persons"
const val CONTACT_LOST_PERSONS_COLLECTION_NAME = "contact"

class FirestoreService {
    val firebaseFirestore = FirebaseFirestore.getInstance()
    val usersCollection = firebaseFirestore.collection(USERS_COLLECTION_NAME)
    val auth = Firebase.auth
    val userID = auth.uid
    var valFinal = 0

    fun getPersonsData(): LiveData<MutableList<PersonaDesaparecida>> {
        val mutableData = MutableLiveData<MutableList<PersonaDesaparecida>>()
        //firebaseFirestore.collection("Desaparecidos").document("id").set()
        firebaseFirestore.collection("Desaparecidos").get().addOnSuccessListener {


            val listData = mutableListOf<PersonaDesaparecida>()
            for (document: QueryDocumentSnapshot in it) {

                Log.d("Persona", "${document.getString("nombre")}")

                val nombre = document.getString("nombre")
                val edad = document.getString("edad")
                val cedula = document.getString("cedula")
                val fechaDesaparicion = document.getString("fechaDesaparicion")
                val sexo = document.getString("sexo")
                val estatura = document.getString("estatura")
                val colorPiel = document.getString("colorPiel")
                val ciudadResidencia = document.getString("ciudadResidencia")
                val rh = document.getString("rh")
                val userImageUrl = document.getString("userImageUrl")
                val person = PersonaDesaparecida(nombre!!,edad!!,cedula!!, fechaDesaparicion!!, sexo!!, estatura!!, colorPiel!!, ciudadResidencia!!, rh!!, userImageUrl!!)
                listData.add(person)

            }
            mutableData.value = listData
        }.addOnFailureListener {
            Log.d("Error", "No hay información -->         ${it.message}")

        }
        Log.d("Datos", " -->    ${mutableData.value}")

        return mutableData

    }

    fun getFoundPersonsData(): LiveData<MutableList<FoundPerson>> {
        val mutableData = MutableLiveData<MutableList<FoundPerson>>()

        firebaseFirestore.collection(FOUND_PERSONS_COLLECTION_NAME).get().addOnSuccessListener {
            val listData = mutableListOf<FoundPerson>()
            for (document: QueryDocumentSnapshot in it) {

                Log.d("FoundPerson", "${document.getString("nombre")}")

                val ubi_encontrado = document.getString("ubi_encontrado")
                val name = document.getString("name")
                val edad = document.getString("edad")
                val fecha_encontrado = document.getString("fecha_encontrado")
                val estatura = document.getString("estatura")
                val num_documento = document.getString("num_documento")
                val picture = document.getString("picture")
                val color_piel = document.getString("color_piel")
                val sexo = document.getString("sexo")
                val address_actual = document.getString("address_actual")
                val phone_contact = document.getString("phone_contact")
                val name_person_report = document.getString("name_person_report")
                val num_doc_person_report = document.getString("num_doc_person_report")
                val tipo_sangre_person_report = document.getString("tipo_sangre_person_report")
                val foundPerson = FoundPerson(
                    ubi_encontrado!!,
                    name!!,
                    edad!!,
                    fecha_encontrado!!,
                    estatura!!,
                    num_documento!!,
                    picture!!,
                    color_piel!!,
                    sexo!!,
                    address_actual!!,
                    phone_contact!!,
                    name_person_report!!,
                    num_doc_person_report!!,
                    tipo_sangre_person_report!!
                )
                listData.add(foundPerson)
            }
            mutableData.value = listData
        }.addOnFailureListener {
            Log.d("Error", "No hay información -->         ${it.message}")
        }
        Log.d("Datos", " -->    ${mutableData.value}")

        return mutableData
    }

    fun setPerson(person: PersonaDesaparecida): Boolean {
        var succes = false
        try {
            firebaseFirestore.collection("Desaparecidos").document(person.cedula).set(person)
            succes = true
        } catch (e: Exception) {
            Log.d("Error", e.message.toString())
        }
        return succes
    }

    fun postFoundPerson(foundPerson: FoundPerson): Boolean {
        var success = false
        try {
            firebaseFirestore.collection(FOUND_PERSONS_COLLECTION_NAME).document(foundPerson.num_documento!!).set(foundPerson)
            success = true
        } catch (e: Exception) {
            Log.d("Error", e.message.toString())
        }
        return success
    }
}



