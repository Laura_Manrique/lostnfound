package com.misiontic.lostandfound.view.ui.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.example.tiendageli.view.ui.network.FirestoreService
import com.example.tiendageli.view.ui.network.USERS_COLLECTION_NAME
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.misiontic.lostandfound.R
import com.misiontic.lostandfound.databinding.ActivityRegisterBinding
import com.misiontic.lostandfound.model.User

class RegisterActivity : AppCompatActivity() {
    private val TAG = "REGISTER_TAG"
    private lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.registerEntityButton?.setOnClickListener {
            createUser()
        }
        binding.loginEntityButton?.setOnClickListener {
            goToLogin()
        }
    }

    fun goToLogin() {
        Log.i(TAG, "goLoginActivity")
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun createUser(){
        val auth = Firebase.auth
        val db = Firebase.firestore

        val fullname = binding.etFullName?.text.toString()
        val numDoc = binding.etDocument?.text.toString()
        val nameEntity = binding.etEntity?.text.toString()
        val address = binding.etAddress?.text.toString()
        val city = binding.etCity?.text.toString()
        val phone = binding.etPhone?.text.toString()

        //valores del nombre en minúsculas
        val name = fullname.lowercase()
        val nameEntityLower = nameEntity.lowercase()
        var userid = ""
        //email igual al nombre de la persona
        val email = name.replace(" ","") + "@${nameEntityLower}.com".replace(" ", "")

        //password igual al numero de documento de la persona
        val password = numDoc

        val user = User()
        user.name = fullname
        user.num_doc = numDoc
        user.name_entidad = nameEntity
        user.address_entidad = address
        user.city = city
        user.phone = phone
        user.email = email

        //TODO: validar que los campos no esten vacios
        if(fullname.isEmpty()){
                binding.etFullName?.error = "Ingrese un nombre y apellido"
                return
            }
        else if(numDoc.isEmpty()){
                binding.etDocument?.error = "Ingrese su numero de documento"
                return
            }
        else if(nameEntity.isEmpty()){
                binding.etEntity?.error = "Ingrese su entidad"
                return
            }
        else if(address.isEmpty()){
                binding.etAddress?.error = "Ingrese su direccion"
                return
            }
        else if(city.isEmpty()){
                binding.etCity?.error = "Ingrese su ciudad"
                return
            }
        else if(phone.isEmpty()){
                binding.etPhone?.error = "Ingrese su telefono"
                return
            }
        else {
                //TODO:Registrar usuario en firebaseAuth
                auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                                val firestore = FirestoreService()
                                val userId = auth.currentUser?.uid
                                userid = userId.toString()

                                //TODO:Registrar usuario en firebaseFirestore
                                db.collection(USERS_COLLECTION_NAME).document(userId.toString())
                                    .set(user)

                                //alerta de registro exitoso
                                AlertDialog.Builder(this)
                                    .setTitle("Registro exitoso")
                                    .setMessage("Bienvenido!! \n su Usuario es: ${user.email} y \n su contraseña es: ${password}.")
                                    .setPositiveButton("Aceptar") { dialog, which ->
                                            goToLogin()
                                        }
                                    .show()

                            } else {
                                //alerta de registro fallido
                                AlertDialog.Builder(this)
                                    .setTitle("Error")
                                    .setMessage("No se pudo registrar el usuario, intente de nuevo")
                                    .setCancelable(true)
                                    .show()
                            }
                    }
            }
        
    }
}